# scan-image

Scan images and SBOM using clamav, grype, and trivy.

## Usage

You should add these components to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: ${CI_SERVER_HOST}/${GITLAB_COMPONENT_GROUP}/scan-image-component/clamav@<VERSION>
    inputs:
      stage: scan-image
      docker-image-tar: "${IMAGE_TAR_PATH}"
  - component: ${CI_SERVER_HOST}/${GITLAB_COMPONENT_GROUP}/scan-image-component/clamav-multi@<VERSION>
    inputs:
      stage: scan-images
      image-tar-folder-path: "${IMAGE_TAR_FOLDER}"
  - component: ${CI_SERVER_HOST}/${GITLAB_COMPONENT_GROUP}/scan-image-component/grype@<VERSION>
    inputs:
      stage: scan-image
      docker-image-tar: "${IMAGE_TAR_PATH}"
  - component: ${CI_SERVER_HOST}/${GITLAB_COMPONENT_GROUP}/scan-image-component/trivy@<VERSION>
    inputs:
      stage: scan-image
```

Replace `<VERSION>` with the required release.

where `${GITLAB_COMPONENT_GROUP}` is a CICD variable with the path to the group containing the gitlab component definitions.

This adds jobs `clamav`, `grype`, and `trivy` to the pipeline.

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `scan-image`      | Optional name to provide for the stage |
| `image` | `<different for each tool>`   | Docker image for the stage |
| `docker-image-tar` | `` | The locally built tarball of the docker image to scan  |
| `image-tar-folder-path` | `` | The path to where image tarballs are stored to scan |
| `sbom-output` | `` | The locally produced SBOM to scan  |

**Note: See [trivy database docs](https://aquasecurity.github.io/trivy/v0.40/docs/vulnerability/db/) for more details.**


## Grype DB Info

Docs are here - https://github.com/anchore/grype/blob/main/README.md#offline-and-air-gapped-environments

### To Update the vulnerabilities DB
update [this file](/templates/grype/template.yml) and the `db-version` input, to propogate the changes through the jobs.

When getting a new vulnerabilities.db use this url to download the latest versions - https://toolbox-data.anchore.io/grype/databases/listing.json

If the pipeline errors with something similar to this message: 
```
grype db import ${ARTIFACT_FOLDER}/grype-db/vulnerability-db_v1_2023-12-05T01:27:07Z_f9127c6142c6897d3f6b.tar.gz
1 error occurred:
	* unable to import vulnerability database: unsupported database version: have=1 want=5
```

Look at the listing.json file for the corresponding version. The json file is organized by version, so all version 1 urls are at the top and all version 5 urls are further down in the file.

## Reports

Your scan reports are stored at `${ARTIFACT_FOLDER}/scan-image/`.

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components
