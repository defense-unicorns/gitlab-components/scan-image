FROM alpine:latest

FROM python:3-alpine
WORKDIR /service

COPY theme.jar .
RUN chmod +rx theme.jar

COPY . ./
EXPOSE 8080
ENTRYPOINT ["python3"]
